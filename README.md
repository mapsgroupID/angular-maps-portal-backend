# Description

This project implements an in memory backend for the [Maps Portal Angular Tutorial][1].

# Requirements

Code should work with any recent version of Node (such as Node 10) or following. I've tested it with Node 12 only, though.

# Install and run

If you have Linux installed:
```bash
# this will install dependencies
npm i
# this will run it
./run.sh
```

If you're using Windows it will work but I don't know how to redirect the output to Bunyan:
```
npm i
npm start
```

Server will start on port 5000 by default. Please refer to application help to customize it:

```
./run.sh --help
# or
npm start -- --help
```

# API

These are the employee related APIs:

* `GET /employees`: will return an array of employees
* `GET /employee/:id`: will return the employee identified by `:id`, where `:id` is the numeric ID of the employee
* `POST /employee`: will create a new employee, pass a properly formatted JSON (see code)
* `PUT /employee/:id`: will update an existing employee identified by `:id`, pass a properly formatted JSON (see code)
* `DELETE /employee/:id`: will delete an existing employee

Some images are also returned at the endpoint `/static/*.jpg`, for instance:
* `/static/andrew_tanenbaum.jpg`



[1]: https://git-maps.maps1.mapsengineering.com/fast/maps-portal