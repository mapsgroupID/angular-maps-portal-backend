'use strict';

const _ = require('lodash');
const util = require('util');
const colors = require('colors/safe');

function isDefined(val) {
  return typeof val !== 'undefined' && val !== null;
}

function isBlank(val) {
  return !isDefined(val) || val.length === 0 || (/^\s*$/).test(val);
}

function isNumber(n) {
  return n !== null && (typeof n === 'number' || n instanceof Number);
}

function parseHostPort(val) {
  const o = {
    host: null,
    port: null
  };
  const i = val.indexOf(':');
  if (i >= 0) {
    const h = val.substr(0, i);
    const p = val.substr(i + 1);
    if (!isBlank(h)) {
      o.host = h;
    }
    if (!isBlank(p)) {
      o.port = parseInt(p, 10);
    }
  } else if (!isBlank(val)) {
    o.host = val;
  }

  return o;
}

function format_red(...args) {
  return colors.red(Reflect.apply(util.format, null, args));
}

function format_ylw(...args) {
  return colors.yellow(Reflect.apply(util.format, null, args));
}

function isString(str, val) {
  return _.isString(str) && str.toLowerCase() === val;
}

function parseBoolean(v) {
  if (v === true || isString(v, 'true') || v === '1' || v === 1) {
    return true;
  }
  if (v === false || isString(v, 'false') || v === '0' || v === 0) {
    return false;
  }
  return null;
}

function parseInt10(s) {
  return parseInt(s, 10);
}

function parseList(v) {
  if (isBlank(v)) {
    return [];
  }

  return v.split(',').filter(s => !isBlank(s));
}

/**
 * Removes one prefix from the string (the first one that matches).
 *
 * @param  {String} str         The string
 * @param  {Array<String>} pfxs The array of prefixes
 * @return {String}             The string without the prefix or the
 *                              original string if none matches.
 */
function unprefix(str, ...pfxs) {
  const pfx = _.find(pfxs, (p) => _.startsWith(str, p));
  return pfx ? pfxs.substr(pfx.length) : null;
}

/**
 * Copies properties from source to target object.
 *
 * @param {Object}        src   Source object
 * @param {Object}        tgt   Target object
 * @param {Array<String>} props Properties to be copied
 * @return {void}
 */
function addProperties(src, tgt, ...props) {
  props.forEach((p) => {
    const val = src[p];
    if (val) {
      tgt[p] = val;
    }
  });
}

function sequence(base = 1) {
  let n = base;
  return () => n++;
}

/**
 * Tests whether an array contains the given value.
 * @param {Array} array An array of simple values that are compared using the equality operator
 * @param {Any} value Any simple value
 * @returns {Boolean} True if array contains the value, false otherwise
 */
function contains(array, value) {
  return array.some(a => a === value);
}

function intersect(arr1, arr2) {
  for (const v1 of arr1) {
    for (const v2 of arr2) {
      if (v1 === v2) {
        return true;
      }
    }
  }
  return false;
}

module.exports = {
  addProperties,
  contains,
  fmt: {
    red: format_red,
    ylw: format_ylw
  },
  isBlank,
  isDefined,
  isNumber,
  intersect,
  parseBoolean,
  parseHostPort,
  parseInt: parseInt10,
  parseList,
  sequence,
  unprefix
};
