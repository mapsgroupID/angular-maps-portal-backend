'use strict';

const bunyan = require('bunyan');
const fs = require('fs');
const path = require('path');

function mkdirSync(p) {
  try {
    fs.mkdirSync(p);
  } catch (e) {
    if (e.code !== 'EEXIST') {
      throw e;
    }
  }
}

/**
 * Creates a new logger.
 * @param {String}  name              Logger name
 * @param {Object}  config            Configuration properties
 * @param {String}  config.dir        Path to logging directory
 * @param {String}  config.level      Logging level
 * @param {number}  config.retention  Number of days log files should be retained
 * @param {boolean} config.stdout     Whether to print to console or not
 * @returns {Object} The logger
 */
function mkLogger(name, config) {
  const { dir, level, retention, stdout } = config;
  mkdirSync(dir);

  const file = path.join(dir, `${name}.log`);
  const logger = bunyan.createLogger({
    name: name,
    src: true,
    level: level,
    streams: [
      {
        type: 'rotating-file',
        path: file,
        period: '1d',
        count: retention,
        level: level
      }
    ]
  });
  if (stdout) {
    logger.addStream({
      name: 'console',
      stream: process.stdout,
      level: level
    });
  }

  /*
   * Note on log rotation: Often you may be using external log rotation utilities like logrotate
   * on Linux or logadm on SmartOS/Illumos. In those cases, unless your are ensuring "copy and truncate"
   * semantics (via copytruncate with logrotate or -c with logadm) then the fd for your 'file' stream
   * will change. You can tell bunyan to reopen the file stream with code like this in your app.
   */
  process.on('SIGUSR2', () => {
    logger.reopenFileStreams();
  });

  return logger;
}

module.exports = { mkLogger };
