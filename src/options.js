'use strict';

const _ = require('lodash');
const { isDefined, parseInt, fmt } = require('./commons');

const BASE_DEFAULT = {
  // logs
  LOG_DIR: './logs',
  LOG_LEVEL: 'debug',
  LOG_RETENTION: 5,
  LOG_STDOUT: true,
  // app
  APP_IFACE: 'localhost',
  APP_PORT: 5000
};

function mkDefault(dflt) {
  return _.merge({}, BASE_DEFAULT, dflt);
}

function setDefault(config, dflt) {
  config.log = _.merge({
    dir: dflt.LOG_DIR,
    level: dflt.LOG_LEVEL,
    retention: dflt.LOG_RETENTION,
    stdout: dflt.LOG_STDOUT,
    throttle: dflt.LOG_THROTTLE
  }, config.log);

  config.app = _.merge({
    iface: dflt.APP_IFACE,
    port: dflt.APP_PORT
  }, config.app);

  return config;
}

/* eslint-disable complexity */
function mergeConfig(config, program) {
  // logging configuration
  config.log = _.merge(config.log, {
    dir: program.logDir,
    level: program.logLevel,
    retention: program.logRetention,
    stdout: isDefined(program.logStdout) ? program.logStdout : config.log.stdout
  });

  // Application REST/WS configuration
  if (program.appPort) { config.app.port = program.appPort; }
  if (program.appIFace) { config.app.iface = program.appIFace; }

  return config;
}

function assertPort(port, program, excode) {
  if (port <= 1024 || port > 65535) {
    console.error(fmt.red('Invalid port (must be between 1025 and 65535): %f', port));
    program.outputHelp();
    process.exit(excode);
  }
}

function parse(custDflt = {}) {
  const { version } = require('../package.json');
  const program = require('commander');
  const dflt = mkDefault(custDflt);

  // generic properties
  program
    .version(version)
    .option('-i, --app-iface <interface>', `The interface the service will listen to for application requests (default ${dflt.APP_IFACE})`)
    .option('-p, --app-port <port>', `The port number the service will listen to for application requests (default ${dflt.APP_PORT})`, parseInt)
    .option('-D, --log-dir <path>', `Path to log directory (default ${dflt.LOG_DIR})`)
    .option('-L, --log-level <level>', `Log level (default ${dflt.LOG_LEVEL})`, parseInt)
    .option('-R, --log-retention <days>', `Retention of logging messages in days (default ${dflt.LOG_RETENTION})`, parseInt)
    .option('-S, --log-stdout', `Logs to stdout as well (default ${dflt.LOG_STDOUT ? 'enabled' : 'disabled'})`);

  // parse
  program.parse(process.argv);

  const config = setDefault({}, dflt);
  mergeConfig(config, program);

  assertPort(config.app.port, program, 2);

  function printableConfig() {
    /* eslint-disable consistent-return */
    return config;
  }

  return {
    config,
    printableConfig,
    name: program.name()
  };
}

module.exports = parse;
