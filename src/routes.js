'use strict';

const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const { sequence, isBlank, isNumber } = require('./commons');

const seq = sequence(1);

const POSITIONS = {
    DEV: 'Software developer',
    ARCH: 'Software architect',
    PM: 'Project manager',
    RES: 'Researcher',
    CTO: 'CTO'
};

const POSITION_VALUES = Object.keys(POSITIONS).map(p => POSITIONS[p]);

class Employee {
    constructor(id, avatarUrl, firstName, lastName, description, age, position, hasEmail, hasPhone) {
        this.id = id;
        this.avatarUrl = avatarUrl;
        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.age = age;
        this.position = position;
        this.hasEmail = hasEmail;
        this.hasPhone = hasPhone;
    }
}

function compareAscByStrings(...props) {
    return function cmp(o1, o2) {
        for (const p of props) {
            const c = o1[p].localeCompare(o2[p]);
            if (c !== 0) {
                return c;
            }
        }
        return 0;
    };
}

const STATIC_PREFIX = '/static/';

const DEFAULT_EMPLOYEES = [
    new Employee(seq(), `${STATIC_PREFIX}linus_torvalds.jpg`, 'Linus', 'Torvalds', 'Creator and, historically, the principal developer of the Linux kernel', 20, POSITIONS.CTO, true, true),
    new Employee(seq(), `${STATIC_PREFIX}andrew_tanenbaum.jpg`, 'Andrew', 'Tanenbaum', 'Dutch / American computer scientist and professor', 30, POSITIONS.RES, true, false),
    new Employee(seq(), `${STATIC_PREFIX}edsger_dijkstra.jpg`, 'Edsger', 'Dijkstra', 'Dutch systems scientist, programmer, software engineer, science essayist, and pioneer', 40, POSITIONS.RES, false, true),
    new Employee(seq(), `${STATIC_PREFIX}richard_stallman.jpg`, 'Richard', 'Stallman', 'American free software movement activist and programmer', 50, POSITIONS.DEV, true, true),
    new Employee(seq(), `${STATIC_PREFIX}dennis_ritchie.jpg`, 'Dennis', 'Ritchie', 'He created the C programming language and the Unix operating system', 60, POSITIONS.PM, false, false),
    new Employee(seq(), `${STATIC_PREFIX}brian_kernighan.jpg`, 'Brian', 'Kernighan', 'Kernighan is coauthor of the AWK and AMPL programming languages', 70, POSITIONS.PM, false, true)
];

function sanitizeAvatarUrl(avatarUrl) {
    return path.join(__dirname, '../public', path.basename(avatarUrl));
}

function isValidAvatar(avatarUrl, log) {
    if (isBlank(avatarUrl)) { return false; }
    const file = sanitizeAvatarUrl(avatarUrl);
    let stat = null;
    try {
        stat = fs.statSync(file);
    } catch (e) {
        log.error(e, `Cannot stat avatar file: ${file}`);
        return false;
    }
    if (!stat.isFile()) { return false; }
    return true;
}

function isValidEmployee(emp, log) {
    if (!emp) {
        log.error('No employee passed');
        return false;
    }
    if (!isValidAvatar(emp.avatarUrl, log)) {
        log.error({ avatarUrl: emp.avatarUrl }, 'Invalid avatar URL');
        return false;
    }
    if (isBlank(emp.firstName) || isBlank(emp.lastName)) {
        log.error('No name provided');
        return false;
    }
    if (!POSITION_VALUES.includes(emp.position)) {
        log.error({ position: emp.position }, 'Invalid position');
        return false;
    }
    if (!isNumber(emp.age)) {
        log.error('No age provided');
        return false;
    }
    return true;
}

function prepareInputEmployee(json, id) {
    return new Employee(
        id,
        `${STATIC_PREFIX}${path.basename(json.avatarUrl)}`,
        json.firstName.substr(0, 50), json.lastName.substr(0, 50),
        json.description ? json.description.substr(0, 200) : null,
        json.age, json.position, json.hasEmail,json.hasPhone
    );
}

function routes(app, log) {
    const employees = DEFAULT_EMPLOYEES.slice();

    app.get('/employees', (req, res) => {
        setTimeout(() => {
            log.info('Loading employees ...');
            let out = employees;
            const { position, count } = req.query;
            if (position) {
                out = out.filter(e => e.position === position);
            }
            if (count) {
                const c = parseInt(count, 10);
                if (c > 0) {
                    out = out.slice(0, c);
                }
            }
            res.json(out.sort(compareAscByStrings('firstName', 'lastName')));
        }, Math.random() * 2000);
    });

    app.post('/employee', (req, res) => {
        let emp = req.body;
        if (!isValidEmployee(emp, log)) {
            res.status(400).json({ error: 'Invalid employee' });
            return;
        }
        log.info(`Saving employee ${emp.id} ...`);
        emp = prepareInputEmployee(emp, seq());
        employees.push(emp);
        setTimeout(() => {
            log.info(`Employee ${emp.id} saved!`);
            res.json({ id: emp.id });
        }, Math.random() * 900);
    });

    app.get('/employee/:id', (req, res) => {
        const id = parseInt(req.params.id, 10);
        log.info(`Loading employee ${id} ...`);
        const employee = employees.find(e => e.id === id);
        if (employee) {
            setTimeout(() => {
                log.info(`Employee ${id} loaded!`);
                res.json(employee);
            }, Math.random() * 600);
            return;
        }
        log.error({ id }, 'No employee exists with that ID');
        res.status(404).json({ error: 'No employee exists with that ID' });
    });

    app.put('/employee/:id', (req, res) => {
        const id = parseInt(req.params.id, 10);
        log.info(`Updating employee ${id} ...`);
        const i = employees.findIndex(e => e.id === id);
        const employee = employees[i];
        if (!employee) {
            log.error({ id }, 'No employee exists with that ID');
            res.status(404).json({ error: 'No employee exists with that ID' });
            return;
        }
        const inEmployee = prepareInputEmployee(req.body, id);
        if (!isValidEmployee(inEmployee, log)) {
            res.status(400).json({ error: 'Invalid employee' });
            return;
        }
        employees[i] = inEmployee;
        setTimeout(() => {
            log.info(`Employee ${id} updated`);
            res.json({ id: employee.id });
        }, Math.random() * 800);
    });

    app.delete('/employee/:id', (req, res) => {
        const id = parseInt(req.params.id, 10);
        log.info(`Deleting employee ${id} ...`);
        const i = employees.findIndex(e => e.id === id);
        const employee = employees[i];
        if (!employee) {
            log.error({ id }, 'No employee exists with that ID');
            res.status(404).json({ error: 'No employee exists with that ID' });
            return;
        }
        employees.splice(i, 1);
        setTimeout(() => {
            log.info(`Employee ${id} deleted`);
            res.json({ id: id });
        }, Math.random() * 500);
    });
}

module.exports = { routes };
