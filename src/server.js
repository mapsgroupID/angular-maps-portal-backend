'use strict';

const util = require('util');
const path = require('path');

// utilities
const rfs = require('rotating-file-stream');
const uuidV4 = require('uuid/v4');

// express
const express = require('express');
const methodOverride = require('method-override');
const bodyParser = require('body-parser');
const compression = require('compression');
const morgan = require('morgan');

// own modules
const opts = require('./options');
const { mkLogger } = require('./logger');
const { routes } = require('./routes');

// creates the configuration options and the logger
const options = opts();
const log = mkLogger('server', options.config.log);
log.debug(options.printableConfig(), 'Configuration merged');

/**
 * Initializes the application middlewares.
 *
 * @param {Object} app Express application
 * @returns {void}
 */
function init(app) {
  app.use(compression());
  app.use(methodOverride());
  app.use(morgan('combined', {
    stream: rfs('access.log', {
      compress: 'gzip', // compress rotated files
      interval: '1d', // rotate daily
      path: options.config.log.dir, // keeps access.log under the log directory
      size: '10M' // rotate every 10 MegaBytes written
    })
  }));

  const pub = path.join(__dirname, '..', 'public');
  app.use('/static', express.static(pub));

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));

  // sets the correlation id of any incoming requests
  app.use((req, res, next) => {
    req.correlationId = req.get('X-Request-ID') || uuidV4();
    res.set('X-Request-ID', req.id);
    next();
  });
}

/**
 * Installs fallback error handlers.
 *
 * @param {Object} app Express application
 * @returns {void}
 */
function fallbacks(app) {

  // generic error handler => err.status || 500 + json
  // NOTE keep the `next` parameter even if unused, this is mandatory for Express 4
  /* eslint-disable no-unused-vars */
  app.use((err, req, res, next) => {
    const errmsg = err.message || util.inspect(err);
    log.error(`Unexpected error occurred while calling ${req.path}: ${errmsg}`);
    res.status(err.status || 500);
    res.json({ error: err.message || 'Internal server error' });
  });

  // if we are here, then there's no valid route => 400 + json
  // NOTE keep the `next` parameter even if unused, this is mandatory for Express 4
  /* eslint-disable no-unused-vars */
  app.use((req, res, next) => {
    log.error(`Route not found to ${req.path}`);
    res.status(404);
    res.json({ error: 'Not found' });
  });
}

// instances the application service (Express and WS)
const app = express();

const { iface, port } = options.config.app;
app.listen(port, iface, () => {
  log.info(`Server listening: http://${iface}:${port}`);
  console.log(`Server listening: http://${iface}:${port}`);
});

init(app);
routes(app, log);
fallbacks(app);
