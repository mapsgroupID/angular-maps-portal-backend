/* eslint-disable object-curly-newline */
/* eslint-disable strict */

function printMe() {
    console.log(`this.x = ${this.x}`);
}

const container = {
    x: 10
};

printMe.apply(container);

Reflect.apply(printMe, container, []);

class AClass {
    constructor() {
        this.x = 200;
    }

    printThis(a, b, c, d) {
        console.log(`this.x = ${this.x}`);
    }

    delayedPrint() {
        setTimeout(this.printThis.bind(container), 1000);
    }
}

new AClass().delayedPrint();

// const a = new AClass();

// Reflect.apply(a.printThis, container, []);


